package crashKurs;

public class Recoursion {

	public static void main(String[] args) {
	    String aa= new String("aaaaaaaaaaa");
//		for (String aa= new String("aaaaaaaaaaa"); aa.length()>0; aa=aa.substring(1)) { // snips off from aa string until nothing left
//			System.out.println("l"+aa+"ngweilig"); //displays "langweilig" 
//		}
	    langweilig(aa);    // true recursion
	}
	private static void langweilig(String aa) {
	    if (aa.length()>0) {
	        System.out.println("l"+aa+"ngweilig");
	        langweilig(aa.substring(1));
	    }
	}
}

